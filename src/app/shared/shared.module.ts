import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from '../components/navbar/navbar.component';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
  ],
  declarations: [
    NavbarComponent
  ],
  providers: [],
  exports: [
    CommonModule,
    HttpClientModule,
    NavbarComponent,
  ]
})
export class SharedModule {
}
