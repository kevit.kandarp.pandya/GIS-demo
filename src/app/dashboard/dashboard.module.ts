import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { GoogleMapsModule } from '@angular/google-maps';
import { DashboardComponent } from './dashboard.component';
import { CustomersComponent } from './customers/customers.component';
import { MapViewComponent } from './customers/map-view/map-view.component';
import { dashboardRoutes } from './dashboard.routing';
import { SharedModule } from '../shared/shared.module';
import { CardViewComponent } from './customers/card-view/card-view.component';

@NgModule({
  declarations: [DashboardComponent, CustomersComponent, MapViewComponent, CardViewComponent],
  imports: [CommonModule, GoogleMapsModule, SharedModule, RouterModule.forChild(dashboardRoutes)],
})
export class DashboardModule {}
