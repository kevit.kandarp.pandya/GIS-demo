import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-map-view',
  templateUrl: './map-view.component.html',
  styleUrls: ['./map-view.component.scss'],
})
export class MapViewComponent implements OnInit {
  @Input() customers: [];
  markers = [];

  constructor() {}

  ngOnInit() {
    this.customers.forEach((cus: any) => {
      this.markers.push({
        // position: {
        //   lat: cus.position.lat + ((Math.random() - 0.5) * 2) / 10,
        //   lng: cus.position.lng + ((Math.random() - 0.5) * 2) / 10,
        // },
        position: cus.position,
        label: {
          color: 'red',
          text: cus.city,
        },
        title: cus.name,
        options: { animation: google.maps.Animation.BOUNCE },
      });
    });
  }
}
