import { Component, OnInit } from '@angular/core';
import * as DATA from '../../../assets/data/customers.json';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent implements OnInit {
  customers = (DATA as any).default;
  view = 'card';

  constructor() {}

  ngOnInit() {}
}
