import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { SnotifyService } from 'ng-snotify';

import { AuthService } from "../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  isProcessing = false;
  buttons = {
    login: {
      btnText: 'Login',
      cssClasses: ['btn-success']
    }
  };

  constructor(
    private router: Router,
    private _authService: AuthService,
    protected snotify: SnotifyService,
  ) { }

  ngOnInit() {
    if (localStorage.getItem("user")) {
      this.router.navigate(["dashboard"]);
    }
    this.loginForm = new FormGroup({
      email: new FormControl("", [Validators.email, Validators.required]),
      password: new FormControl("", Validators.required)
    });
  }

  // LOGIN
  async login() {
    if (!this.loginForm.invalid) {
      this.changeButtonState('login', 'processing');
      const credentials = {
        email: this.loginForm.value.email.trim(),
        password: this.loginForm.value.password.trim()
      }
      try {
        const res: any = await this._authService.login(credentials);
        this.changeButtonState('login', 'process');
        if (res) {
          this.snotify.success('Login successfully');
          this.router.navigateByUrl("dashboard");
        } else {
          this.changeButtonState('login', 'process');  
        }
      } catch (error) {
        this.changeButtonState('login', 'process');
        console.error('error:::::', error, ':::::error');
        this.snotify.error('Something went wrong');
      }
    }
  }

  // CHANGE STATE OF BUTTON BASED ON EVENT
  changeButtonState(of: string, state: string) {
    switch (of) {
      case 'login':
        if (state === 'process') {
          this.buttons[of].cssClasses = ['btn-success'];
          this.buttons[of].btnText = 'Login';
          this.isProcessing = false;
        } else if (state === 'processing') {
          this.buttons[of].cssClasses = ['btn-success', 'btn-stripped', 'active-stripes'];
          this.buttons[of].btnText = 'Login';
          this.isProcessing = true;
        } else if (state === 'error') {
          this.buttons[of].cssClasses = ['btn-danger'];
          this.buttons[of].btnText = 'Login';
          this.isProcessing = false;
        }
        break;
      default:
        break;
    }
  }
}
