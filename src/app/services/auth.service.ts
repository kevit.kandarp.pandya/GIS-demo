import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { SnotifyService } from 'ng-snotify';

import * as DATA from '../../assets/data/admins.json';

@Injectable()
export class AuthService {
  admins = (DATA as any).default;
  isLogin: boolean;

  constructor(protected snotify: SnotifyService, private router: Router) {
    this.isLogin = localStorage.getItem('user') ? true : false;
  }

  login(credentials) {
    let userStatus = 'User not found';
    this.admins.forEach((adm) => {
      if (adm.email === credentials.email) {
        if (adm.password === credentials.password) {
          userStatus = 'User authorized';
          return;
        } else {
          userStatus = 'Invalid credentials';
          return;
        }
      }
    });
    if (userStatus === 'User authorized') {
      localStorage.setItem('user', credentials.email);
      this.isLogin = true;
      return true;
    } else {
      this.snotify.error(userStatus);
      return false;
    }
  }

  logout() {
    localStorage.removeItem('user');
    this.isLogin = false;
    this.router.navigate(['login']);
  }
}
